import https from "https";
import http from "http";

import epxpress from "express";
import InitMongo from "./inits/initMongo";
import InitExpress from "./inits/initExpress";

var fs = require("fs");
var path = require("path");

const serverType = process.env.SERVER_TYPE;
const NODE_ENV = process.env.NODE_ENV;

const runApp = async () => {
  try {
    let server = null;
    await InitMongo();
    const app = InitExpress();
    server = http.createServer(app);
    server.listen(3001, () => console.log(`Listening on port ${3001}`));
    //Run services
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
};

module.exports = runApp;
