import BaseController from "./base";
import AuthService from "../services/auth_service";
import bcrypt from "bcrypt";

import BadRequestException from '../errors/bad-request';

class AuthController extends BaseController {
  accountService = new AuthService();
  constructor() {
    super();
  }
  async register(req, res, next) {
    try {
      const data = req.body;
      // Validation input
      if (!data.email || data.email === '') throw new BadRequestException('EMAIL_MUST_BE_NOT_EMPTY');
      if (!data.username || data.username === '') throw new BadRequestException('USERNAME_MUST_BE_NOT_EMPTY');
      if (!data.password || data.password === '') throw new BadRequestException('PASSWORD_MUST_BE_NOT_EMPTY');

      if (!this.isValidEmail(data.email)) throw new BadRequestException('EMAIL_IS_INVALID');

      // Check duplicate info
      const isDupEmail = await this.accountService.isDuplicateData('email', data.email);
      if (isDupEmail) throw new BadRequestException('EMAIL_IS_ALREADY_TAKEN');

      const isDupUsername = await this.accountService.isDuplicateData('username', data.username);
      if (isDupUsername) throw new BadRequestException('USERNAME_IS_ALREADY_TAKEN');

      // Crypt pass
      const hash = bcrypt.hashSync(data.password, '$2b$04$2uXRjOrtjYMP3NY/ksrQJe');
      hash;

      // New docs
      const acc = await this.accountService.create({
        ...data,
        password: hash,
      });

      return res.json({ isSuccess: true, info: acc });
    } catch (error) {
      // console.log(error)
      return res.status(500).json({ isSuccess: false, message: error.message });
    }
  }

  isValidEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
}

export default AuthController;
