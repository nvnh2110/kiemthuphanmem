import BaseAutoBind from "../base/BaseAutoBind";

class BaseController extends BaseAutoBind {
  constructor() {
    super();
    if (new.target === BaseController) {
      throw new TypeError("Cannot construct BaseController instances directly");
    }
  }
}
export default BaseController;
