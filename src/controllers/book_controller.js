import BaseController from "./base";
import BookService from "../services/book_service";

import BadRequestException from '../errors/bad-request';

class BookController extends BaseController {
  bookService = new BookService();
  constructor() {
    super();
  }

  async create(req, res, next) {
    try {
      let { name, price, code, quantity } = req.body;

      // Check input
      if (!name || name === '') throw new BadRequestException('BOOK_NAME_MUST_BE_NOT_EMPTY');
      if (!price || price === '') throw new BadRequestException('BOOK_PRICE_MUST_BE_NOT_EMPTY');
      if (!code || code === '') throw new BadRequestException('BOOK_CODE_MUST_BE_NOT_EMPTY');
      if (!quantity || quantity === '') throw new BadRequestException('BOOK_QUANTITY_MUST_BE_NOT_EMPTY');

      if (typeof price !== 'number' || price < 0) throw new BadRequestException('BOOK_PRICE_MUST_BE_GREATER_THAN_0');
      if (typeof quantity !== 'number' || quantity < 0) throw new BadRequestException('BOOK_QUANTITY_MUST_BE_GREATER_THAN_0');
      if (code.length < 5) throw new BadRequestException('THE_LENGTH_OF_THE_BOOK_CODE_MUST_BE_GREATER_THAN_5');
      if(!this.isCodeValid(code)) throw new BadRequestException('THE_BOOK_CODE_MUST_BE_NOT_START_BY_A_NUMBER');

      // Check input is not already taken
      const isDupName = await this.bookService.isDuplicateData('name', name);
      if (isDupName) throw new BadRequestException('BOOK_NAME_IS_ALREADY_TAKEN');

      const isDupCode = await this.bookService.isDuplicateData('code', code);
      if (isDupCode) throw new BadRequestException('BOOK_CODE_IS_ALREADY_TAKEN');

      // Insert to db
      const newBook = await this.bookService.create(req.body);
      return res.json({ isSuccess: true, data: newBook });
    } catch (error) {
      return res.json({ isSuccess: false, message: error.message });
    }
  }

  isCodeValid(string) {
    var reg = /^\d+$/;
    const first = string.slice(0, 1);
    const sec = string.slice(1, 2);
    return !reg.test(String(first).toLowerCase()) && !reg.test(String(sec).toLowerCase());
  }
}

export default BookController;
