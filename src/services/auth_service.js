import ValidationError from "../errors/validation";
import validator from "validator";
import sha256 from "sha256";
import Base from "./base";

//Import models
import UserModel from "../models/User";

class AuthService extends Base {
  async create(data) {
    const created = await UserModel.create({
      ...data,
   });
    return created;
  }

  async isDuplicateData(field, value) {
    const isAvailable = await UserModel.find({
      [field]: value,
    });
    if (isAvailable.length > 0) {
      return true;
    } else {
      return false;
    }
  }
}
export default AuthService;
