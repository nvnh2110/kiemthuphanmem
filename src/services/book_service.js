import ValidationError from "../errors/validation";
import validator from "validator";
import sha256 from "sha256";
import Base from "./base";

//Import models
import BookModel from "../models/Book";

class BookService extends Base {
  async create(data) {
    const created = await BookModel.create({
      ...data,
   });
    return created;
  }

  async isDuplicateData(field, value) {
    const isAvailable = await BookModel.find({
      [field]: value,
    });
    if (isAvailable.length > 0) {
      return true;
    } else {
      return false;
    }
  }
}
export default BookService;
