/**
 * Created by crosp on 5/11/17.
 */

'use strict';
const BaseError = require('./base');

class BadRequestException extends BaseError {
    constructor(message) {
        super(message, 409);
    }
}

module.exports = BadRequestException;