const router = require("express").Router();

// Import files routers
import Auth from "./auth";
import Book from "./book";

// Create use routers
router.use("/auth", Auth);
router.use("/book", Book);

// router.use('/', (req, res, next) => { res.json({message: 'welcome'}) })
export default router;
