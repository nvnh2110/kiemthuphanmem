const router = require("express").Router();

//import middleware
//import controller
import BookController from "../../controllers/book_controller";
let bookController = new BookController();

router.post("/create", bookController.create);

export default router;
