const router = require("express").Router();

//import middleware
//import controller
import AuthController from "../../controllers/auth_controller";
let authController = new AuthController();

router.post("/register", authController.register);

export default router;
