import mongoose from "mongoose";

const BookSchema = new mongoose.Schema(
  {
    name: String,
    quantity: Number,
    price: Number,
    code: String,
  },
  { timestamps: true }
);
const BookModel = mongoose.model("Book", BookSchema);
export default BookModel;
