import mongoose from "mongoose";

const UserSchema = new mongoose.Schema(
  {
    username: String,
    password: String,
    name: String,
    email: String,
  },
  { timestamps: true }
);
const UserModel = mongoose.model("Account", UserSchema);
export default UserModel;
