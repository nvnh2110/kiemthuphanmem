require("babel-polyfill")
require("babel-register")

var chai = require('chai')
var supertest = require('supertest');
var app = require('../inits/initExpress').default;
var mongoInit = require('../inits/initMongo').default;

const request = supertest(app());
mongoInit();

describe('TestCase: POST /auth/register', async () => {
  it('Receive message: EMAIL_MUST_BE_NOT_EMPTY', async () => {
    //Call API
    let testCase = await request
      .post('/v1/auth/register')
      .set('Accept', 'application/json');

    //Data test
    chai.expect(testCase.body.message).to.match(/EMAIL_MUST_BE_NOT_EMPTY/)
  })

  it('Receive message: USERNAME_MUST_BE_NOT_EMPTY', async () => {
    //Call API
    let testCase = await request
      .post('/v1/auth/register')
      .send({ email: 'nhathoang.ntct@gmail.com' })
      .set('Accept', 'application/json');

    //Data test
    chai.expect(testCase.body.message).to.match(/USERNAME_MUST_BE_NOT_EMPTY/)
  })

  it('Receive message: PASSWORD_MUST_BE_NOT_EMPTY', async () => {
    //Call API
    let testCase = await request
      .post('/v1/auth/register')
      .send({ email: 'nhathoang.ntct@gmail.com', username: 'nhathoang' })
      .set('Accept', 'application/json');

    //Data test
    chai.expect(testCase.body.message).to.match(/PASSWORD_MUST_BE_NOT_EMPTY/)
  })

  it('Receive message: EMAIL_IS_INVALID', async () => {
    //Call API
    let testCase = await request
      .post('/v1/auth/register')
      .send({ email: 'nhathoang.ntct', username: 'nhathoang', password: '123123' })
      .set('Accept', 'application/json');

    //Data test
    chai.expect(testCase.body.message).to.match(/EMAIL_IS_INVALID/)
  })

  it('Receive message: USERNAME_IS_ALREADY_TAKEN', async () => {
    //Call API
    let testCase = await request
      .post('/v1/auth/register')
      .send({ email: 'nhathoang.ntct@gmail.com', username: 'nhathoang', password: '123123' })
      .set('Accept', 'application/json');

    //Data test
    chai.expect(testCase.body.message).to.match(/USERNAME_IS_ALREADY_TAKEN/)
  })

  it('Receive message: USERNAME_IS_ALREADY_TAKEN', async () => {
    //Call API
    let testCase = await request
      .post('/v1/auth/register')
      .send({ email: 'nhathoang.ntct1@gmail.com', username: 'nhathoang2', password: '123123' })
      .set('Accept', 'application/json');

    //Data test
    chai.expect(testCase.body.isSuccess).to.match(true)
    chai.expect(testCase.body.message).to.match(/USERNAME_IS_ALREADY_TAKEN/)
  })
})
